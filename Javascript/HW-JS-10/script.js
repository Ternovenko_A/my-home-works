const error = document.querySelector('.error');
const icon = document.querySelectorAll('.icon-password');
const len = icon.length;
let i = 0;
error.style.display = 'none';

/*Скрываем/показываем пароль по клику на иконку*/
for (let i =0; i < len; i++){
    icon[i].onclick = function(){
        let target = this;
        if(target.className === 'fas fa-eye icon-password'){
            showPassword(target);
        }else{
            hidePassword(target);
        }
    }
}

function showPassword(show) {
    show.previousElementSibling.type = "text";
    show.className = "fas fa-eye-slash icon-password";
}

function hidePassword(hide) {
    hide.previousElementSibling.type = "password";
    hide.className = "fas fa-eye icon-password";
}

/*Обработчик Кнопки*/
btn.onclick = ()=> {
    error.style.color = 'red';
    let password1 = document.getElementById("firstPas").value;
    let password2 = document.getElementById("secondPas").value;
    if(password1 === password2){
        error.style.display = 'none';
        alert('You are welcome');
    }else{
        error.style.display = 'block';
    }
};

