
$('.tabs-title')
    .on('click', (e) => {
    $('.tabs-title').removeClass('active');
    $(e.target).addClass('active');
    const data = $(e.target).data('content');
    console.log(data);
    $('.tabs-content-item').hide();
    $('.tabs-content').find(`[data-content = ${data}]`).show();
});
