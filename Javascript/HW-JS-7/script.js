const userText = prompt("Enter your text", "Знаете, любой дурак может огородить кусок пляжа забором, Но не у каждого получится собственная республика, Разве что дача или закусочная с танцевальным майданчиком Чтобы заставить других поверить в то чего нет Нужно найди очень правильные слова, а не просто какие то: бла-бла-бла");
const arr = userText.split(" ");


/*========Вариант реализации через method map()========*/

function newArr(){
   const fragment = document.createDocumentFragment();
   const ul = document.createElement("ul");
   this.map(liText => {
   let li = document.createElement('li');
      /*добавил шаблонную стр*/
   let textInLi = document.createTextNode(`${liText}`);
   li.appendChild(textInLi);
   fragment.appendChild(li);
});
ul.appendChild(fragment);
document.body.insertBefore(ul, document.querySelector('script'));
}

newArr(arr);


/*========Вариант реализации через method forEach()========*/
// function newArr(array){
// let fragment = document.createDocumentFragment();
// const ul = document.createElement("ul");
//    array.forEach(function (liText) {
//    let li = document.createElement('li');
//    let textInLi = document.createTextNode(liText);
//    li.appendChild(textInLi);
//    fragment.appendChild(li);
// });
// ul.appendChild(fragment);
// document.body.insertBefore(ul, document.querySelector('script'));
// };
//
//newArr(arr);

/*========Вариант реализации через цикл for========*/
// function NewArr(array){
//    let fragment = document.createDocumentFragment();
//    let i=0;
//    const ul = document.createElement("ul");
//    for(i; i<array.length; i++) {
//        let li = document.createElement('li');
//        li.appendChild(document.createTextNode(`${array[i]}`));
//        fragment.appendChild(li);
//    }
//    ul.appendChild(fragment);
//    document.body.insertBefore(ul, document.querySelector('script'));
// };
//
//newArr(arr);


/*Вариант реализации через цикл for of*/
// function NewArr(array){
//    let fragment = document.createDocumentFragment();
//    let i=0;
//    const ul = document.createElement("ul");
//    for(i of array) {
//        let li = document.createElement('li');
//        li.appendChild(document.createTextNode(i));
//        fragment.appendChild(li);
//    }
//    ul.appendChild(fragment);
//    document.body.insertBefore(ul, document.querySelector('script'));
// };
//
//newArr(arr);
