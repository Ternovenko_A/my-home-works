
const input  = document.getElementById('price');/*создаю переменную для <icon id = "price"*/
const topMessage = document.getElementById('result');/*создаю переменную для <div  id = "result"*/
const ifError = document.getElementById('error');/*создаю переменную для <p id="error"*/
const closeBtn = document.getElementById('close'); /*создаю переменную для <span id="close"*/
const inputMessage = document.getElementById('inputText');/*создаю переменную для <span id="inputText"*/

input.onblur = function() {

    if(this.value < 0 || isNaN(this.value)){
        ifError.innerText = "Please enter correct price"; /*добавляю в тег р текст об ошибке. innerText нахрен удаляет все что внутри + другие теги, потом перезаписывает то что мы ему говорим*/
        topMessage.style.display = "none";
        ifError.style.display = "block";
        this.style.cssText = "boxSizing: borderBox; border: 2px solid red; outline: none;";
    }else if(this.value>=0){
        topMessage.style.display = "block";
        inputMessage.innerText = `Текущая цена: ${input.value}`;
        ifError.style.display = "none";
        this.style.cssText = "boxSizing: borderBox; border: 2px solid green; outline: none;";
    }
};

/*Ф-ция активируется при клике на <span id="close". благодаря onclick. аргумент ф-ции = event (прочитать)*/
closeBtn.onclick = function (event) {
        event.target.parentNode.style.display = "none"; /*Стучимся через event  к <div  id = "result". чтоб убрать его с поля зрения. */
        input.value = ""; /*очищаем значения в теге icon*/
};