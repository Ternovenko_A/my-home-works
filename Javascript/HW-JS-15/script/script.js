
const anchorLinks = $("#anchorLinks");
const $btnTop = $(".top-button");

anchorLinks.on('click', 'a', function (event) {
    let linkName = $(this).attr('href');
    let height = $(linkName).offset().top;
    $('body, html').animate({scrollTop: height}, 900)
});

$(window).on("scroll", function () {
    console.log($(window).scrollTop());
    if($(window).scrollTop() >= 985){
        $btnTop.fadeIn();
    }else{
        $btnTop.fadeOut();
    }
});

$btnTop.on('click', function () {
    $("html,body").animate({scrollTop:0},650)
});
