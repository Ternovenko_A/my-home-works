1. присвоил переменные для querySelectorAll для всех <li class="tabs-title" и <li class="tabs-content-item show". чтоб потом их можно было перебирать через forEach().--
2. Прописал ф--цию 
tabs.onclick = (e) => {
    let target = e.target; **-- вещаю переменну на конкретное событие--**
    tabTitle.forEach((elem) => { **-- благодаря этой функции могу клацать на один пункт таба, а другой будет затираться. Касается только <li class="tabs-title"--**
       elem.classList.remove('active');
    });
    if(target.className === 'tabs-title') { **--Если у переменной target  есть класс 'tabs-title',то выполнить условие ниже**
       target.classList.add('active');  **-- присваиваю активному элементу класс 'active'.**
       let dataset = target.dataset.content; **- делаю переменную со значением текущего data-content ="..."--**
       tabContent.forEach((elem) => {
           elem.classList.remove('active'); **-- удаляю все видимые 'tabs-content-item'--**
           if(elem.dataset.content === `${dataset}` ){ **если dataset`ы совпадают, делат код ниже:--**
               elem.classList.add('active'); **присваиваю для 'tabs-content-item' класс 'active'**
           }
       });
    
    }
};