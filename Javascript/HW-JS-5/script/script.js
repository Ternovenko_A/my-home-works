function createNewUser() {

 const newUser = {
    firstName: prompt("Enter your name", "Sam"),
    lastName: prompt("Enter your last name", "Dorn"),
    birthday: prompt("Enter your date of birthday", "10.11.1983"),
    getLogin: function () {
            return this.firstName[0].toLowerCase()+ this.lastName.toLowerCase();
    },
    getAge: function(){
        let dd = this.birthday.slice(0,2);
        let mm = this.birthday.slice(3,5) - 1;
        let yyyy = this.birthday.slice(6,10);
        let dateOfBirthday = new Date(yyyy, mm, dd);
        let age = (new Date() - dateOfBirthday)/31536000000;

        // return age|0;  /*Тот же вариант что и ниже, с испольхованием побитового или*/
        // return Math.floor(age); /*Тот же вариант что и ниже, с испольхованием Математического метода*/
        return ~~age;  /*Тот же вариант что и ниже, с испольхованием двойного оператора побитового отрицания*/
    },
    getPassword: function(){
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase()+this.birthday.slice(6,10);
    }
};
return newUser;
}


const userSam = createNewUser();


console.log(userSam);
console.log("You are "  + userSam.getAge() + " years old");
console.log("Your recommended password is: " +  userSam.getPassword());